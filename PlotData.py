#!/usr/bin/env python2.7
"""
Date:     190513
Author:   Danika MacDonell
Purpose:  Plotting code for MonoSWW 1-lepton signal region, to be used as a base for sensitivity studies.
          Currently, this code is largely a conversion of the ROOT plotting macro written by Stanislava Sevova to python.

          Original ROOT macro: https://gitlab.cern.ch/ssevova/mono-s-quick-plotting/blob/master/plotMonoS_CR1.C
"""

from root_numpy import root2array
import matplotlib.pyplot as plt
import numpy as np
import math as math
import sys as sys
import uproot_methods
from skhep.math.vectors import LorentzVector
from matplotlib.gridspec import GridSpec
from matplotlib.pyplot import *
from skhep.visual import MplPlotter as skh_plt
plt.rc('xtick', labelsize=20)
plt.rc('ytick', labelsize=20)

# Changing font to stix; setting specialized math font properties as directly as possible
rcParams['font.family'] = 'arial'
rcParams['mathtext.fontset'] = 'custom'
rcParams['mathtext.bf'] = 'serif:italic:bold'

############################### Constants Defined Here #####################################
LUMI = 36.1         # Luminosity of the data
Bkgs = ["Dijets", "VHbb", "Zjets", "Diboson", "ttbar", "Wjets"]
SubBkgs = [["dijet"], ["VHbb"], ["Zee_cl", "Zee_hf", "Zee_hpt", "Zee_l", "Zmumu_cl", "Zmumu_hf", "Zmumu_hpt", "Zmumu_l", "Ztautau_cl", "Ztautau_hf", "Ztautau_hpt", "Ztautau_l", "Znunu_cl", "Znunu_hf", "Znunu_hpt", "Znunu_l"], ["WW", "WZ", "ZZ"], ["ttbar"], ["Wmunu_cl", "Wmunu_hf", "Wmunu_hpt", "Wmunu_l", "Wenu_cl", "Wenu_hf", "Wenu_hpt", "Wenu_l", "Wtaunu_cl", "Wtaunu_hf", "Wtaunu_hpt", "Wtaunu_l"]]
Bkg_colours = ["magenta", "purple", "yellow", "orange", "green", "cyan"]
#Bkgs = ["dijet", "VHbb"]
#SubBkgs = [["dijet"], ["VHbb"]]
#Bkg_colours = ["magenta", "purple"]

Signals = ["zp1000_dm200_dh160", "zp1000_dm300_dh260", "zp2500_dm200_dh160"]
SubSignals = [["monoSww_all_zp1000_dm200_dh160"], ["monoSww_all_zp1000_dm300_dh260"], ["monoSww_all_zp2500_dm200_dh160"]]
Sig_colours = ["red", "blue", "brown"]
############################################################################################

# Function to convert a list of 1D arrays to a 2D array
def arr_list_to_2darr(v, fillval=0.):
  lens = np.array([len(item) for item in v])
  mask = lens[:,None] > np.arange(lens.max())
  out = np.full(mask.shape,fillval)
  out[mask] = np.concatenate(v)
  return out

# Function to find the first nonzero element in each row of a 2D array
def first_nonzero(arr, invalid_val=0):
  mask = arr!=0   # Boolean array with zero elements false and all other elements true
  
  # Return an array with the column index of the first nonzero value in each row, or zero if all columns in a row are equal to zero
  return np.where(mask.any(axis=1), mask.argmax(axis=1), invalid_val)

# Define a function to calculate the difference between two angles such that it's always between +/-pi
def delta_phi(phi1, phi2):
  result = phi1-phi2
  if result > math.pi: result = result - 2*math.pi
  elif result < -math.pi: result = result + 2*math.pi
  return result

# Class to make histograms for each variable
class Histogram:

  def __init__(self, name, Region, xlabel, binning, do_log=False):
    self.name = name
    self.Region = Region
    self.xlabel = xlabel
    self.binning = binning
    self.do_log = do_log
    self.bkgs = []
    self.weights = []
    self.bkg_names = []
    self.sigs = []
    self.sig_names = []

  def add_sample(self, name, data, type, weights=1):
    if type == "Bkg":
      self.bkg_names.append(name)
      self.bkgs.append(data)
      self.weights.append(weights)

    elif type == "Sig":
      self.sig_names.append(name)
      self.sigs.append(data)

  def plot_hist(self):
    gs = GridSpec(3, 1)  # 3 rows, 1 column

    # Main plot shows the background and amplified signal distributions
    fig = plt.figure(figsize =(12, 11))
    ax1=fig.add_subplot(gs[0:2,0])
    ax1.get_xaxis().set_visible(False)
    ax1.set_ylabel("Events / %.2f" % (self.binning[1] - self.binning[0]), fontsize=20)
    plt.figtext(0.12, 0.92, r"$\mathbf{ATLAS}$ Internal", fontsize=22)
    plt.figtext(0.12, 0.89, "SR 1L (%s)"%Region, fontsize=18)
    plt.figtext(0.12, 0.86, "$\sqrt{s}$ = 13 TeV, %.1f fb$^{-1}$"%LUMI, fontsize=18)

    # Lower panel shows the signal over background
    ax2 = fig.add_subplot(gs[2, 0], sharex=ax1)
    ax2.set_xlabel(self.xlabel, fontsize=20)
    ax2.set_ylabel("Signal / Background", fontsize=20)

    # Plot the background, signal, and signal / background if we've collected the expected number of backgrounds
    if len(self.bkgs) == len(Bkgs):
      bkg_hist, bin_edges, bkg_error, patches = skh_plt.hist(self.bkgs, stacked=True, label=self.bkg_names, bins=self.binning, color = Bkg_colours, weights = self.weights, errorbars=True, err_return=True, err_color="dimgray", ax=ax1)
      bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])
      bkg_hist_total = np.zeros(len(bkg_hist[0]))

      # Collect the total background histogram and combine all the data and weights
      for iBkg in np.arange(len(bkg_hist)):
        bkg_hist_total += bkg_hist[iBkg]

      # Set the y-axis range based on the max background bin amplitude
      if self.do_log: ax1.set_ylim(0.01, 100.*max(bkg_hist_total))
      else: ax1.set_ylim(0.0001, 1.5*max(bkg_hist_total))

      for iSig in np.arange(len(Signals)):
        sig_hist, bin_edges, patches = ax1.hist(self.sigs[iSig], label=self.sig_names[iSig], bins=self.binning, color=Sig_colours[iSig], histtype='step', weights=np.ones(len(self.sigs[iSig])), linewidth=3)
        ratio_hist = 1.0*sig_hist / bkg_hist_total
        ratio_hist[(ratio_hist==np.nan) | (ratio_hist==np.inf) | (ratio_hist < 0)] = 0.
        ratio_err = ratio_hist*np.sqrt((1./np.sqrt(sig_hist))**2 + (bkg_error/bkg_hist_total)**2)
        ratio_err[(ratio_err==np.nan) | (ratio_err==np.inf) | (ratio_hist < 0)] = 0.
        ax2.errorbar(bin_centers, ratio_hist, yerr = ratio_err, drawstyle = 'steps-mid', color=Sig_colours[iSig], linewidth=2, capsize=4)

      ax1.legend(prop={'size': 18}, ncol=2)

      if self.do_log and max(bkg_hist_total) > 0: ax1.set_yscale('log')
    plt.tight_layout()
    plt.savefig("Plots/%s_%s.png"%(self.name, self.Region))
    plt.close()

# Read in the selection to plot
Region = "None"
if len(sys.argv)>1:
  if sys.argv[1] == '0':
    print("------> Plotting Merged Region")
    Region = "Merged"
  elif sys.argv[1] == '1':
    print("------> Plotting Intermediate Region")
    Region = "Intermediate"
  elif sys.argv[1] == '2':
    print("------> Plotting Resolved Region")
    Region = "Resolved"
  else:
    print("------> Invalid input! Must be an integer from 0 to 2")
    exit()

else:
  print("------> Plotting Merged Region")
  Region = "Merged"

# Initialize a dictionary with the histograms to be plotted
HistDict = {
'N_SignalMuons': Histogram('N_SignalMuons', Region, "N (signal muons)", np.linspace(0,10,11)),
'N_SignalElectrons': Histogram('N_SignalElectrons', Region, "N (signal electrons)", np.linspace(0,10,11)),
'N_Bjets': Histogram('N_Bjets', Region, "N (b-jets)", np.linspace(0,10,11)),
'N_Jets04': Histogram('N_Jets04', Region, "N (small-R jets)", np.linspace(0,10,11)),
'N_Jets10': Histogram('N_Jets10', Region, "N (large-R jets)", np.linspace(0,10,11)),
'N_fwdjets': Histogram('N_fwdjets', Region, "N (small-R fwd jets)", np.linspace(0,10,11)),
'met': Histogram('met', Region, "E$_\mathrm{T}^\mathrm{miss}$", np.linspace(150, 2000, 51)),
'metLog': Histogram('metLog', Region, "E$_\mathrm{T}^\mathrm{miss}$", np.linspace(150, 2000, 51), do_log=True),
'dPhiMin3': Histogram('dPhiMin3', Region, "$\Delta\phi_\mathrm{min}$($\mathrm{jet}_{1-3}$, $E_\mathrm{T}^\mathrm{miss}$)", np.linspace(0, math.pi, 21)),
'dPhiMin3Log': Histogram('dPhiMin3Log', Region, "$\Delta\phi_\mathrm{min}$($\mathrm{jet}_{1-3}$, $E_\mathrm{T}^\mathrm{miss}$)", np.linspace(0, math.pi, 21), do_log=True),
'dPhiJJ': Histogram('dPhiJJ', Region, "$\Delta\phi(\mathrm{jet}_1,\mathrm{jet}_2)$", np.linspace(0, math.pi, 21)),
'dPhiJJLog': Histogram('dPhiJJLog', Region, "$\Delta\phi(\mathrm{jet}_1,\mathrm{jet}_2)$", np.linspace(0, math.pi, 21), do_log=True),
'dRJJ': Histogram('dRJJ', Region, "$\Delta\mathrm{R}(\mathrm{jet}_1,\mathrm{jet}_2)$", np.linspace(0, 5, 21)),
'dRJJLog': Histogram('dRJJLog', Region, "$\Delta\mathrm{R}(\mathrm{jet}_1,\mathrm{jet}_2)$", np.linspace(0, 5, 21), do_log=True),
'dPhiMETJJ': Histogram('dPhiMETJJ', Region, "$\Delta\phi(\mathrm{E}_\mathrm{T}^\mathrm{miss},\mathrm{jet}_1+\mathrm{jet}_2)$", np.linspace(0, math.pi, 21)),
'dPhiMETJJLog': Histogram('dPhiMETJJLog', Region, "$\Delta\phi(\mathrm{E}_\mathrm{T}^\mathrm{miss},\mathrm{jet}_1+\mathrm{jet}_2)$", np.linspace(0, math.pi, 21), do_log=True),
'HTRatioResolved': Histogram('HTRatioResolved', Region, "H$_\mathrm{T}$ ratio (resolved)", np.linspace(0, 2, 21)),
'HTRatioResolvedLog': Histogram('HTRatioResolvedLog', Region, "H$_\mathrm{T}$ ratio (resolved)", np.linspace(0, 2, 21), do_log=True),
'HTRatioMerged': Histogram('HTRatioMerged', Region, "H$_\mathrm{T}$ ratio (merged)", np.linspace(0, 2, 21)),
'HTRatioMergedLog': Histogram('HTRatioMergedLog', Region, "H$_\mathrm{T}$ ratio (merged)", np.linspace(0, 2, 21), do_log=True),
'JetsSumPt': Histogram('JetsSumPt', Region, "$\sum p_{\mathrm{T,jets}}$ [GeV]", np.linspace(0, 2000, 51)),
'JetsSumPtLog': Histogram('JetsSumPtLog', Region, "$\sum p_{\mathrm{T,jets}}$ [GeV]", np.linspace(0, 2000, 51), do_log=True),
'leadFatJet_m': Histogram('leadFatJet_m', Region, "lead Jet M [GeV]", np.linspace(0, 500, 51)),
'leadFatJet_mLog': Histogram('leadFatJet_mLog', Region, "lead Jet M [GeV]", np.linspace(0, 500, 51), do_log=True),
'leadFatJet_pt': Histogram('leadFatJet_pt', Region, "lead Jet p$_\mathrm{T}$ [GeV]", np.linspace(0, 1000, 51)),
'leadFatJet_ptLog': Histogram('leadFatJet_ptLog', Region, "lead Jet p$_\mathrm{T}$ [GeV]", np.linspace(0, 1000, 51), do_log=True),
'leadSmallJet_m': Histogram('leadSmallJet_m', Region, "lead SmallJet M [GeV]", np.linspace(0, 200, 21)),
'leadSmallJet_mLog': Histogram('leadSmallJet_mLog', Region, "lead SmallJet M [GeV]", np.linspace(0, 200, 21), do_log=True),
'leadSmallJet_pt': Histogram('leadSmallJet_pt', Region, "lead SmallJet p$_\mathrm{T}$ [GeV]", np.linspace(0, 800, 41)),
'leadSmallJet_ptLog': Histogram('leadSmallJet_ptLog', Region, "lead SmallJet p$_\mathrm{T}$ [GeV]", np.linspace(0, 800, 41), do_log=True),
'leadFatJet_tau32': Histogram('leadFatJet_tau32', Region, r"lead FatJet $\tau_{32}$", np.linspace(0, 1, 26)),
'leadFatJet_tau32Log': Histogram('leadFatJet_tau32Log', Region, r"lead FatJet $\tau_{32}$", np.linspace(0, 1, 26), do_log=True),
'leadFatJet_tau21': Histogram('leadFatJet_tau21', Region, r"lead FatJet $\tau_{21}$", np.linspace(0, 1, 26)),
'leadFatJet_tau21Log': Histogram('leadFatJet_tau21Log', Region, r"lead FatJet $\tau_{21}$", np.linspace(0, 1, 26), do_log=True),
'leadFatJet_tauD2': Histogram('leadFatJet_tauD2', Region, "lead FatJet D2", np.linspace(0, 25, 26)),
'leadFatJet_tauD2Log': Histogram('leadFatJet_tauD2Log', Region, "lead FatJet D2", np.linspace(0, 25, 26), do_log=True),
'mWCand': Histogram('mWCand', Region, "W candidate M [GeV]", np.linspace(0, 140, 21)),
'mWCandLog': Histogram('mWCandLog', Region, "W candidate M [GeV]", np.linspace(0, 140, 21), do_log=True),
'min3jmass': Histogram('min3jmass', Region, "min M$_\mathrm{jjj}$", np.linspace(0, 250, 51)),
'min3jmassLog': Histogram('min3jmassLog', Region, "min M$_\mathrm{jjj}$", np.linspace(0, 250, 51), do_log=True),
'min_FatSmallJetInvMass': Histogram('min_FatSmallJetInvMass', Region, "min (large-R + small-R) jet M [GeV]", np.linspace(0, 300, 61)),
'min_FatSmallJetInvMassLog': Histogram('min_FatSmallJetInvMassLog', Region, "min (large-R + small-R) jet M [GeV]", np.linspace(0, 300, 61), do_log=True),
'min_FatSmallJJInvMass': Histogram('min_FatSmallJJInvMass', Region, "min (large-R + 2 small-R) jet M [GeV]", np.linspace(0, 300, 61)),
'min_FatSmallJJInvMassLog': Histogram('min_FatSmallJJInvMassLog', Region, "min (large-R + 2 small-R) jet M [GeV]", np.linspace(0, 300, 61), do_log=True),
'min_FatSmallJJJInvMass': Histogram('min_FatSmallJJJInvMass', Region, "min (large-R + 3 small-R) jet M [GeV]", np.linspace(0, 300, 61)),
'min_FatSmallJJJInvMassLog': Histogram('min_FatSmallJJJInvMassLog', Region, "min (large-R + 3 small-R) jet M [GeV]", np.linspace(0, 300, 61), do_log=True),
}

# Function to collect data for all the background/signal samples
def fill_samples(names, sub_names, type):
  # Loop through the backgrounds
  for iSample in np.arange(len(names)):

    # Raw quantities
    N_SignalMuons = np.zeros(0)
    N_SignalElectrons = np.zeros(0)
    N_Bjets = np.zeros(0)
    N_Jets04 = np.zeros(0)
    N_Jets10 = np.zeros(0)
    N_fwdjets = np.zeros(0)
    met = np.zeros(0)
    dPhiMin3 = np.zeros(0)
    dPhiJJ = np.zeros(0)
    dRJJ = np.zeros(0)
    dPhiMETJJ = np.zeros(0)
    HTRatioResolved = np.zeros(0)
    HTRatioMerged = np.zeros(0)
    JetsSumPt = np.zeros(0)
    eventNumber = np.zeros(0)

    # Computed quantities
    weights = np.zeros(0)
    leadSmallJet_m = np.zeros(0)
    leadSmallJet_pt = np.zeros(0)
    leadFatJet_m = np.zeros(0)
    leadFatJet_pt = np.zeros(0)
    leadFatJet_tau32 = np.zeros(0)
    leadFatJet_tau21 = np.zeros(0)
    leadFatJet_tauD2 = np.zeros(0)
    min_FatSmallJetInvMass = np.zeros(0)
    min_FatSmallJJInvMass = np.zeros(0)
    min_FatSmallJJJInvMass = np.zeros(0)
    min_Mjj = np.zeros(0)
    min_Mjjj = np.zeros(0)
    mWCand = np.zeros(0)
    min3jmass = np.zeros(0)

    # Read in the data
    for iSubSample in np.arange(len(sub_names[iSample])):
      print("Handling sample %s of type %s"%(sub_names[iSample][iSubSample], type))
      try: data = root2array("FlatTrees/%s.root"%sub_names[iSample][iSubSample], "%s_Nominal"%sub_names[iSample][iSubSample]).view(np.recarray)
      except IOError:
        print("-----> Skipping empty sample %s"%sub_names[iSample][iSubSample])
        continue

      nEvents = len(data.Jet_m)

      # Calculate the weights
      weights = np.append(weights, data.JetWeight*data.MuoWeight*data.EleWeight*data.MET_TriggerSF*data.prwWeight*data.NormWeight*LUMI)

      ########################## Collect raw quantities ##########################
      N_SignalMuons = np.append(N_SignalMuons, data.N_SignalMuons)
      N_SignalElectrons = np.append(N_SignalElectrons, data.N_SignalElectrons)
      N_Bjets = np.append(N_Bjets, data.N_BJets_04)
      N_Jets04 = np.append(N_Jets04, data.N_Jets04)
      N_Jets10 = np.append(N_Jets10, data.N_Jets10)
      N_fwdjets = np.append(N_fwdjets, data.N_ForwardJets04)
      met = np.append(met, data.MetTST_met)
      dPhiMin3 = np.append(dPhiMin3, data.DeltaPhiMin3)
      dPhiJJ = np.append(dPhiJJ, data.DeltaPhijj)
      dRJJ = np.append(dRJJ, data.DeltaRjj)
      dPhiMETJJ = np.append(dPhiMETJJ, data.DeltaPhiMetjj)
      HTRatioResolved = np.append(HTRatioResolved, data.HtRatioResolved)
      HTRatioMerged = np.append(HTRatioMerged, data.HtRatioMerged)
      JetsSumPt = np.append(JetsSumPt, data.sigjet012ptsum)
      eventNumber = np.append(eventNumber, data.eventNumber)
      ############################################################################

      ####################### Collect computed quantities ######################
      # Leading small-R jet
      # Convert all small-R subjets to 2d arrays
      smallJet_m_arr = arr_list_to_2darr(data.Jet_m)
      smallJet_pt_arr = arr_list_to_2darr(data.Jet_pt)
      smallJet_eta_arr = arr_list_to_2darr(data.Jet_eta)
      smallJet_phi_arr = arr_list_to_2darr(data.Jet_phi)

      # Get indices of leading small-R jets (first nonzero subjet for each small-R jet)
      leadSmallJets_idx = first_nonzero(smallJet_m_arr)

      # Get leading mass and pt of small-R jets
      leadSmallJet_m_comp = smallJet_m_arr[np.arange(nEvents), leadSmallJets_idx]
      leadSmallJet_pt_comp = smallJet_pt_arr[np.arange(nEvents), leadSmallJets_idx]

      # Leading fat jet arrays
      fatJet_m_arr = arr_list_to_2darr(data.FatJet_m)
      fatJet_pt_arr = arr_list_to_2darr(data.FatJet_pt)
      fatJet_eta_arr = arr_list_to_2darr(data.FatJet_eta)
      fatJet_phi_arr = arr_list_to_2darr(data.FatJet_phi)

      # Get indices of leading fat jets (first nonzero subjet for each small-R jet)
      leadFatJets_idx = first_nonzero(fatJet_m_arr)

      # Get variables of interest for the leading fat jet
      leadFatJet_m_comp = fatJet_m_arr[np.arange(nEvents), leadFatJets_idx]
      leadFatJet_pt_comp = fatJet_pt_arr[np.arange(nEvents), leadFatJets_idx]
      leadFatJet_tau32_comp = arr_list_to_2darr(data.FatJet_tau32_wta)[np.arange(nEvents), leadFatJets_idx]
      leadFatJet_tau21_comp = arr_list_to_2darr(data.FatJet_tau21_wta)[np.arange(nEvents), leadFatJets_idx]
      leadFatJet_tauD2_comp = arr_list_to_2darr(data.FatJet_d2)[np.arange(nEvents), leadFatJets_idx]

      # Reco candidate arrays
      recoCand_m_arr = arr_list_to_2darr(data.RecoCandidates_m)
      recoCand_pt_arr = arr_list_to_2darr(data.RecoCandidates_pt)
      recoCand_eta_arr = arr_list_to_2darr(data.RecoCandidates_eta)
      recoCand_phi_arr = arr_list_to_2darr(data.RecoCandidates_phi)
      recoCand_constit_arr = arr_list_to_2darr(data.RecoCandidates_constituents)


      # For each event, find the smallest invariant mass of various combinations of fat jets with other types of jets
      # DMM: why is this info of interest???
      min_FatSmallJetInvMass_comp = 999. * np.ones(nEvents)
      min_FatSmallJJInvMass_comp = 999. * np.ones(nEvents)
      min_FatSmallJJJInvMass_comp = 999. * np.ones(nEvents)
      min_Mjj_comp = 999. * np.ones(nEvents)
      min_Mjjj_comp = 999. * np.ones(nEvents)

      for iFat in np.arange(len(fatJet_m_arr[0,:])):
        # Make a Lorentz 4-vector from the Fat Jet pt, eta, phi, and m
        vFatJet_col = uproot_methods.TLorentzVectorArray.from_ptetaphim(fatJet_pt_arr[:,iFat], fatJet_eta_arr[:,iFat], fatJet_phi_arr[:,iFat], fatJet_m_arr[:,iFat])

        for iSmall in np.arange(len(smallJet_m_arr[0,:])):
          # Make a small-R jet Lorentz 4-vector
          vSmallJet_col = uproot_methods.TLorentzVectorArray.from_ptetaphim(smallJet_pt_arr[:,iSmall], smallJet_eta_arr[:,iSmall], smallJet_phi_arr[:,iSmall], smallJet_m_arr[:,iSmall])

          # Calculate the angular separation between the given large-R and small-R jets
          DeltaR = np.sqrt(vSmallJet_col.delta_r2(vFatJet_col))

          # Calculate the invariant mass between the small-R and fat jet
          FatSmallJetInvMass = np.sqrt((vFatJet_col + vSmallJet_col).mass2)

          # If the given small-R fat jet combo gives the smallest invariant mass so far and the angular separation is more than 0.2,
          # update the minimum invariant mass to this value. Otherwise, keep it at its original value
          min_FatSmallJetInvMass_comp = np.where( (FatSmallJetInvMass < min_FatSmallJetInvMass_comp) & (DeltaR >= 0.2), FatSmallJetInvMass, min_FatSmallJetInvMass_comp)

        # Combine with smallest Mjj or Mjjj RecoCandidate
        for iReco in np.arange(len(recoCand_m_arr[0,:])):
          # Make a reco candidate Lorentz 4-vector
          vRecoCand_col = uproot_methods.TLorentzVectorArray.from_ptetaphim(recoCand_pt_arr[:,iReco], recoCand_eta_arr[:,iReco], recoCand_phi_arr[:,iReco], recoCand_m_arr[:,iReco])

          # Determine the number of reconstructed candidates
          RecoCand_constit_col = recoCand_constit_arr[:, iReco]

          # Calculate the angular separation between the fat jet and the reco candidate jet
          DeltaR_nj = vRecoCand_col.delta_r2(vFatJet_col)

          # Calculate the minimum mJJ/mJJJ
          Mnj = np.sqrt(vRecoCand_col.mass2)     # Invariant mass for any number n of reco jet candidates
          min_Mjj_comp = np.where( (RecoCand_constit_col==2) & (Mnj < min_Mjj_comp) & (DeltaR_nj >= 0.2), Mnj, min_Mjj_comp)
          min_Mjjj_comp = np.where( (RecoCand_constit_col==3) & (Mnj < min_Mjjj_comp) & (DeltaR_nj >= 0.2), Mnj, min_Mjjj_comp)

          # Calculate the invariant mass between fat and small-R jets for either JJ or JJJ
          # reco candidates
          FatSmallnJInvMass = np.sqrt((vFatJet_col + vRecoCand_col).mass2)
          min_FatSmallJJInvMass_comp = np.where( (RecoCand_constit_col==2) & (FatSmallnJInvMass < min_FatSmallJJInvMass_comp) & (DeltaR_nj >= 0.2), FatSmallnJInvMass, min_FatSmallJJInvMass_comp)
          min_FatSmallJJJInvMass_comp = np.where( (RecoCand_constit_col==3) & (FatSmallnJInvMass < min_FatSmallJJJInvMass_comp) & (DeltaR_nj >= 0.2), FatSmallnJInvMass, min_FatSmallJJJInvMass_comp)

      # Reco candidate combinatorics

      # Determine the mass of the W candidate (i.e. the mass of the jet closest to 80.4 GeV) in the event that there are 2 reco jets
      recoCand_m_arr_compare = np.copy(recoCand_m_arr)
      recoCand_m_arr_compare[(recoCand_m_arr_compare==0)|(recoCand_constit_arr!=2)] = 999.      # Set any zero masses or events with different than 2 reco jets to unreasonably high numbers

      # Check first to make sure there are any reco candidates to work with
      mWCand_comp = 999. * np.ones(nEvents)
      min3jmass_comp = 999. * np.ones(nEvents)
      try:
        mWCand_idx = np.argmin(np.abs(recoCand_m_arr_compare-80.4), axis=1)
        mWCand_comp = recoCand_m_arr[np.arange(nEvents), mWCand_idx]

        # Determine the minimum jet mass in events with 3 reconstructed jets
        recoCand_m_arr_compare = np.copy(recoCand_m_arr)
        recoCand_m_arr_compare[(recoCand_m_arr_compare==0)|(recoCand_constit_arr!=3)] = 999.      # Set any zero masses or events with different than 3 reco jets to unreasonably high numbers
        min3jmass_comp = np.min(recoCand_m_arr_compare, axis=1)

      except ValueError:
        pass

      # Save all computed quantities
      leadSmallJet_m = np.append(leadSmallJet_m, leadSmallJet_m_comp)
      leadSmallJet_pt = np.append(leadSmallJet_pt, leadSmallJet_pt_comp)
      leadFatJet_m = np.append(leadFatJet_m, leadFatJet_m_comp)
      leadFatJet_pt = np.append(leadFatJet_pt, leadFatJet_pt_comp)
      leadFatJet_tau32 = np.append(leadFatJet_tau32, leadFatJet_tau32_comp)
      leadFatJet_tau21 = np.append(leadFatJet_tau21, leadFatJet_tau21_comp)
      leadFatJet_tauD2 = np.append(leadFatJet_tauD2, leadFatJet_tauD2_comp)
      min_FatSmallJetInvMass = np.append(min_FatSmallJetInvMass, min_FatSmallJetInvMass_comp)
      min_FatSmallJJInvMass = np.append(min_FatSmallJJInvMass, min_FatSmallJJInvMass_comp)
      min_FatSmallJJJInvMass = np.append(min_FatSmallJJJInvMass, min_FatSmallJJJInvMass_comp)
      min_Mjj = np.append(min_Mjj, min_Mjj_comp)
      min_Mjjj = np.append(min_Mjjj, min_Mjjj_comp)
      mWCand = np.append(mWCand, mWCand_comp)
      min3jmass = np.append(min3jmass, min3jmass_comp)
      ############################################################################

    # Define merged, intermediate, and resolved regions
    cRegion = True
    cMerged = (N_Jets10 == 1)&(met > 500)
    cIntermediate = ( (~cMerged)&(N_Jets10 == 1) ) | (N_Jets10 > 1)
    cResolved = (~cMerged)&(~cIntermediate)&(N_Jets04 >= 2)
    if Region == "Merged": cRegion = cMerged
    if Region == "Intermediate": cRegion = cIntermediate
    if Region == "Resolved": cRegion = cResolved

    # Remove event in W+jets sample with large negative weight
    if names[iSample] == "Wjets": cBadEvent = (eventNumber != 5111385)
    else: cBadEvent = True

    # Add the data to the histogram class for the given background
    HistDict['N_SignalMuons'].add_sample(names[iSample], N_SignalMuons[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['N_SignalElectrons'].add_sample(names[iSample], N_SignalElectrons[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['N_Bjets'].add_sample(names[iSample], N_Bjets[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['N_Jets04'].add_sample(names[iSample], N_Jets04[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['N_Jets10'].add_sample(names[iSample], N_Jets10[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['N_fwdjets'].add_sample(names[iSample], N_fwdjets[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['met'].add_sample(names[iSample], met[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['metLog'].add_sample(names[iSample], met[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['dPhiMin3'].add_sample(names[iSample], dPhiMin3[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['dPhiMin3Log'].add_sample(names[iSample], dPhiMin3[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['dPhiJJ'].add_sample(names[iSample], dPhiJJ[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['dPhiJJLog'].add_sample(names[iSample], dPhiJJ[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['dRJJ'].add_sample(names[iSample], dRJJ[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['dRJJLog'].add_sample(names[iSample], dRJJ[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['dPhiMETJJ'].add_sample(names[iSample], dPhiMETJJ[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['dPhiMETJJLog'].add_sample(names[iSample], dPhiMETJJ[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['HTRatioResolved'].add_sample(names[iSample], HTRatioResolved[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['HTRatioResolvedLog'].add_sample(names[iSample], HTRatioResolved[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['HTRatioMerged'].add_sample(names[iSample], HTRatioMerged[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['HTRatioMergedLog'].add_sample(names[iSample], HTRatioMerged[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['JetsSumPt'].add_sample(names[iSample], JetsSumPt[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['JetsSumPtLog'].add_sample(names[iSample], JetsSumPt[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_m'].add_sample(names[iSample], leadFatJet_m[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_mLog'].add_sample(names[iSample], leadFatJet_m[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_pt'].add_sample(names[iSample], leadFatJet_pt[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_ptLog'].add_sample(names[iSample], leadFatJet_pt[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadSmallJet_pt'].add_sample(names[iSample], leadSmallJet_pt[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadSmallJet_ptLog'].add_sample(names[iSample], leadSmallJet_pt[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadSmallJet_m'].add_sample(names[iSample], leadSmallJet_m[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadSmallJet_mLog'].add_sample(names[iSample], leadSmallJet_m[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_tau32'].add_sample(names[iSample], leadFatJet_tau32[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_tau32Log'].add_sample(names[iSample], leadFatJet_tau32[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_tau21'].add_sample(names[iSample], leadFatJet_tau21[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_tau21Log'].add_sample(names[iSample], leadFatJet_tau21[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_tauD2'].add_sample(names[iSample], leadFatJet_tauD2[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['leadFatJet_tauD2Log'].add_sample(names[iSample], leadFatJet_tauD2[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['mWCand'].add_sample(names[iSample], mWCand[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['mWCandLog'].add_sample(names[iSample], mWCand[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['min3jmass'].add_sample(names[iSample], min3jmass[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['min3jmassLog'].add_sample(names[iSample], min3jmass[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['min_FatSmallJetInvMass'].add_sample(names[iSample], min_FatSmallJetInvMass[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['min_FatSmallJetInvMassLog'].add_sample(names[iSample], min_FatSmallJetInvMass[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['min_FatSmallJJInvMass'].add_sample(names[iSample], min_FatSmallJJInvMass[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['min_FatSmallJJInvMassLog'].add_sample(names[iSample], min_FatSmallJJInvMass[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['min_FatSmallJJJInvMass'].add_sample(names[iSample], min_FatSmallJJJInvMass[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])
    HistDict['min_FatSmallJJJInvMassLog'].add_sample(names[iSample], min_FatSmallJJJInvMass[cRegion&cBadEvent], type, weights[cRegion&cBadEvent])

# Fill background samples
fill_samples(Bkgs, SubBkgs, "Bkg")

# Fill signal samples
fill_samples(Signals, SubSignals, "Sig")

# Plot all the backgrounds for each variable
for key, Hist in HistDict.items():
  print("--------> Plotting hist %s"%key)
  Hist.plot_hist()
