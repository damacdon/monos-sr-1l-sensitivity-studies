# Python tools for basic MonoSWW SR 1L Sensitivity Studies

## 1. Flat ntuple Plotting

The script PlotData.py produces some quick plots of the SR 1L background and signals with weights and statistical uncertainty, as well as signal / bkg ratios in a lower panel.

### Prerequisites
1. Python2.7 or 3.6 with numpy and matplotlib
2. root_numpy: pip install root_numpy
3. uproot_methods: pip install uproot_methods
4. skhep: pip install scikit-hep


### How to Use
To run this script, first make a soft link named 'FlatTrees' to a directory containing the flat ntuples produced by WriteDefaultTrees in the XAMPPMonoS package. 

Eg:

~~~~
ln -s ../SR1L FlatTrees
~~~~

Then, execute the plotting script with an integer argument representing the region to be plotted.

0: Merged region
1: Intermediate region
2: Resolved region

Eg. 

~~~~
./PlotData.py 0         # This would produce plots in the 'Plots' directory for the merged region
~~~~
